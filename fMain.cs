﻿using FWHWproject.lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FWHWproject
{
    public partial class fMain : Form
    {
        #region Vars
        my_pc hardwareStatus_ = new my_pc();
        float? presentedRAM = -1;
        List<fTemCirc> monitores = new List<fTemCirc>();
        #endregion
        public fMain()
        {
            InitializeComponent();
        }
        private void fMain_Load(object sender, EventArgs e)
        {
            presentedRAM = (new openhwmon()).presentedRAM();
        }
        private void timerData_Tick(object sender, EventArgs e)
        {
            refreshData();
        }
        private void refreshData()
        {
            hardwareStatus_ = new openhwmon().get_();
            float? aux_ = 0;
            for(int i = 0; i < monitores.Count(); i++)
            {
                switch (monitores[i].Name)
                {
                    case "cpu_temp":
                        {
                            aux_ = hardwareStatus_.cputemp;
                            break;
                        }
                    case "cpu_power":
                        {
                            aux_ = hardwareStatus_.cpupower;
                            break;
                        }
                    case "cpu_load":
                        {
                            aux_ = hardwareStatus_.cpuload;
                            break;
                        }
                    case "cpu_freq":
                        {
                            aux_ = hardwareStatus_.cpufreq;
                            break;
                        }
                    case "ram_load":
                        {
                            aux_ = hardwareStatus_.ramload;
                            break;
                        }
                    case "ram_usage":
                        {
                            aux_ = hardwareStatus_.ramused;
                            break;
                        }
                    case "gpu_temp":
                        {
                            aux_ = hardwareStatus_.gputemp;
                            break;
                        }
                    case "gpu_power":
                        {
                            aux_ = hardwareStatus_.gpupower;
                            break;
                        }
                    case "gpu_load":
                        {
                            aux_ = hardwareStatus_.gpuload;
                            break;
                        }
                    case "gpu_freq":
                        {
                            aux_ = hardwareStatus_.gpufreq;
                            break;
                        }
                    case "vram_freq":
                        {
                            aux_ = hardwareStatus_.vramfreq;
                            break;
                        }
                } monitores[i].set_value(aux_);
            }
        }
        #region BTNS
        private void btnINFO_Click(object sender, EventArgs e)
        {
            (new aboutZequi()).Show();
        }
        private void btnCloseALL_Click(object sender, EventArgs e)
        {
            if (monitores.Count > 0)
            {
                for (int i = 0; i < monitores.Count; i++) monitores[i].Close();
                monitores.Clear();
                timerData.Enabled = false;
                MessageBox.Show("ALL WINDOWS CLOSE", "DONE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else MessageBox.Show("There's no windows to close", "NOPE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        #endregion
        #region CTRL_MONITORS
        private void new_monitor(string type_)
        {
            bool flag_ = false;
            for (int i = 0; i < monitores.Count; i++)
            {
                if (monitores[i].Name == type_) flag_ = !flag_;
            }
            if (!flag_)
            {
                data_mon auxPC = (new hardwareSpecs()).monitoring_(type_);
                if (type_ == "ram_usage") auxPC.max = Convert.ToInt16(presentedRAM);
                timerData.Enabled = true;
                fTemCirc newForm = new fTemCirc(type_, auxPC);
                newForm.Show();
                monitores.Add(newForm);
            }
            else MessageBox.Show("Is already open " + type_, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        private void close_monitor(string type_)
        {
            for (int i = 0; i < monitores.Count; i++)
            {
                if (monitores[i].Name == type_)
                {
                    monitores[i].Close();
                    monitores.RemoveAt(i);
                }
            }
            if (monitores.Count == 0) timerData.Enabled = false;
        }
        #endregion  
        #region CPU
        #region TEMP
        private void btncton_Click(object sender, EventArgs e)
        {
            new_monitor("cpu_temp");
        }
        private void btnctoff_Click(object sender, EventArgs e)
        {
            close_monitor("cpu_temp");
        }
        #endregion
        #region POWER
        private void btncpon_Click(object sender, EventArgs e)
        {
            new_monitor("cpu_power");
        }
        private void btncpoff_Click(object sender, EventArgs e)
        {
            close_monitor("cpu_power");
        }
        #endregion

        #region LOAD
        private void btncuon_Click(object sender, EventArgs e)
        {
            new_monitor("cpu_load");
        }

        private void btncuoff_Click(object sender, EventArgs e)
        {
            close_monitor("cpu_load");
        }

        #endregion

        #region FREQ
        private void btncfon_Click(object sender, EventArgs e)
        {
            new_monitor("cpu_freq");
        }

        private void btncfoff_Click(object sender, EventArgs e)
        {
            close_monitor("cpu_freq");
        }
        #endregion

        #endregion
        #region RAM
        #region LOAD
        private void btnrlon_Click(object sender, EventArgs e)
        {
            new_monitor("ram_load");
        }

        private void btnrloff_Click(object sender, EventArgs e)
        {
            close_monitor("ram_load");
        }

        #endregion

        #region USAGE
        private void btnruon_Click(object sender, EventArgs e)
        {
            new_monitor("ram_usage");
        }

        private void btnruoff_Click(object sender, EventArgs e)
        {
            close_monitor("ram_usage");
        }
        #endregion

        #endregion
        #region GPU
        #region TEMP
        private void btngton_Click(object sender, EventArgs e)
        {
            new_monitor("gpu_temp");
        }

        private void btngtoff_Click(object sender, EventArgs e)
        {
            close_monitor("gpu_temp");
        }

        #endregion

        #region POWER
        private void btngpon_Click(object sender, EventArgs e)
        {
            new_monitor("gpu_power");
        }

        private void btngpoff_Click(object sender, EventArgs e)
        {
            close_monitor("gpu_power");
        }

        #endregion

        #region USAGE
        private void btnguon_Click(object sender, EventArgs e)
        {
            new_monitor("gpu_usage");
        }

        private void btnguoff_Click(object sender, EventArgs e)
        {
            close_monitor("gpu_usage");
        }

        #endregion

        #region LOAD
        private void btnglon_Click(object sender, EventArgs e)
        {
            new_monitor("gpu_load");
        }

        private void btngloff_Click(object sender, EventArgs e)
        {
            close_monitor("gpu_load");
        }

        #endregion

        #region FREQ
        private void btngfon_Click(object sender, EventArgs e)
        {
            new_monitor("gpu_freq");
        }

        private void btngfoff_Click(object sender, EventArgs e)
        {
            close_monitor("gpu_freq");
        }

        #endregion

        #region VRAM FREQ
        private void btnvramfon_Click(object sender, EventArgs e)
        {
            new_monitor("vram_freq");
        }

        private void btnvramfoff_Click(object sender, EventArgs e)
        {
            close_monitor("vram_freq");
        }
        #endregion

        #endregion
    }
}

#region EXPLAINS
/*
|-> timerData:
|---|--> Interval: it's cpu ticks, which 100 ticks is 100ms or 1000 ticks is 1s


*/
#endregion