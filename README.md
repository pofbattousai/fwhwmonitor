<h1>Fairwarning Hardware Monitor Project {FWHWmonitor}</h1>
<hr />
<p>This project was build by PofBattousai for the streamer Fairwarning17 (<a href="twitch.tv/fairwarning17" target="_blank" rel="noopener">Twitch</a>).</p>
<p>This project is attach to GNU GPLv3.</p>
<hr />
<h3>Donation and Support are always welcome!</h3>
<p>Feel free to support or donate to devs for more content and work.</p>
<p>Where can I donate or support?</p>
<ul>
<li><a href="https://streamlabs.com/pofbattousai/tip">https://streamlabs.com/pofbattousai/tip</a></li>
<li>UALA Only for <span style="text-decoration: underline;"><strong>ARGENTINA</strong></span></li>
</ul>
<p><img src="https://panels-images.twitch.tv/panel-193742559-image-e7c4e2c9-ad94-440f-8479-8a86d9de1509" alt="" width="320" height="513" /></p>
<hr />
<p>Requirements for use:</p>
<ul>
<li>.NET Framework 4.7.2 or greater</li>
<li>Windows 7 or greater</li>
<li>At least a dual core CPU</li>
<li>2GB or more RAM</li>
<li>Motherboards mid o high end</li>
<li>Hardware greater than 2012 (The older the machine, the less sensor the system has)</li>
</ul>
<hr />
<p>Requirements to code:</p>
<ul>
<li>Windows 7 or greater</li>
<li>Dual core CPU</li>
<li>4gb or more RAM</li>
<li><a href="https://visualstudio.microsoft.com/es/downloads/" target="_blank" rel="noopener">Visual Studio 2019 Community</a>
<ul>
<li>Please read on Microsoft Visual Studio, the requirements to run the IDE</li>
</ul>
</li>
<li><a href="https://openhardwaremonitor.org/downloads/" target="_blank" rel="noopener">OpenHardwareMonitor</a>
<ul>
<li>The OpenHardwareMonitorLib.dll is include in my project but you can redownload if by any chance that one doesn't work</li>
</ul>
</li>
</ul>
<p>The software is free to use and open to contribution!</p>