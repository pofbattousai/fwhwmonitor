﻿using System;
using OpenHardwareMonitor.Hardware; //OpenHardwareMonitorLib.dll

namespace FWHWproject.lib
{
    struct my_pc{
        #region CPU
        public int cputemp;
        public int cpufreq;
        public int cpuvolts;
        public int cpupower;
        public int cpuload;
        #endregion
        #region RAM
        public float? ramused;
        public float? ramavailable;
        public float? ramload;
        #endregion
        #region GPU
        public int gputemp;
        public int gpuvolts;
        public int gpufreq;
        public int vramfreq;
        public int gpupower;
        public int gpuload;
        #endregion
        public int floatqtointq(float? in_)
        {
            int transVal = Convert.ToInt16(in_);
            return transVal;
        }
    }
    class openhwmon
    {
        my_pc miPC = new my_pc();
        my_pc myPCmonitoring()
        {
            #region
            UpdateVisitor updateVisitor = new UpdateVisitor();
            Computer computer = new Computer();
            computer.Open();
            computer.CPUEnabled = true;
            computer.GPUEnabled = true;
            computer.RAMEnabled = true;
            computer.Accept(updateVisitor);
            #endregion
            my_pc miPC = new my_pc();
            for (int i = 0; i < computer.Hardware.Length; i++)
            { // Watts = Volts * Ampere => Ampere = Watts / Volts
                if (computer.Hardware[i].HardwareType == HardwareType.CPU) //CPU DATA RECOLLECTION
                {
                    for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                    {
                        switch (computer.Hardware[i].Sensors[j].SensorType)
                        {
                            case SensorType.Temperature: // CPU TEMP (ºC)
                                {
                                    miPC.cputemp = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                            case SensorType.Voltage: //CPU VOLTS (V)
                                {
                                    if (computer.Hardware[i].Sensors[j].Name == "CPU Total") miPC.cpuvolts = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                            case SensorType.Power: //CPU POWER (W)
                                {
                                    if (computer.Hardware[i].Sensors[j].Name == "CPU Package") miPC.cpupower = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                            case SensorType.Clock: //CPU FREQ || CLOCK (MHZ)
                                {
                                    if (computer.Hardware[i].Sensors[j].Name != "Bus Speed") miPC.cpufreq = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                            case SensorType.Load: //CPU LOAD (%)
                                {
                                    miPC.cpuload = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                        }
                    }
                } 
                else if (computer.Hardware[i].HardwareType == HardwareType.RAM) //RAM DATA RECOLLECTION
                {
                    for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                    {
                        switch (computer.Hardware[i].Sensors[j].SensorType)
                        {
                            case SensorType.Data: //READ DATA RELATED TO RAM PRESENTED ON SYSTEM
                                {
                                    if (computer.Hardware[i].Sensors[j].Name == "Used Memory") //RAM OCCUPIED IN THE MOMENT
                                        miPC.ramused = computer.Hardware[i].Sensors[j].Value;
                                    break;
                                }
                            case SensorType.Load:
                                {
                                    miPC.ramload = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                        }
                    }
                }
                else if (computer.Hardware[i].HardwareType == HardwareType.GpuAti || computer.Hardware[i].HardwareType == HardwareType.GpuNvidia) //GPU DATA RECOLLECTION
                {
                    for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                    {
                        switch (computer.Hardware[i].Sensors[j].SensorType)
                        {
                            case SensorType.Temperature: //GPU TEMP (ºC)
                                {
                                    miPC.gputemp = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                            case SensorType.Voltage: //GPU VOLT (V)
                                {
                                    miPC.gpuvolts = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                            case SensorType.Power: //GPU POWER (W)
                                {
                                    miPC.gpupower = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                            case SensorType.Clock: //GPU CLOCK || FREQ && VRAM CLOCK || FREQ  (MHZ)
                                {
                                    if (computer.Hardware[i].Sensors[j].Name == "GPU Core") miPC.gpufreq = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    if (computer.Hardware[i].Sensors[j].Name == "GPU Memory") miPC.vramfreq = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                            case SensorType.Load: //GPU LOAD (%)
                                {
                                    miPC.gpuload = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                        }
                    }
                }
            }
            computer.Close();
            return miPC;
        }
        public float? presentedRAM()
        {
            #region
            UpdateVisitor updateVisitor = new UpdateVisitor();
            Computer computer = new Computer();
            computer.Open();
            computer.RAMEnabled = true;
            computer.Accept(updateVisitor);
            #endregion
            for (int i = 0; i < computer.Hardware.Length; i++)
            { // Watts = Volts * Ampere => Ampere = Watts / Volts
                if (computer.Hardware[i].HardwareType == HardwareType.RAM)
                {
                    for (int j = 0; j < computer.Hardware[i].Sensors.Length; j++)
                    {
                        switch (computer.Hardware[i].Sensors[j].SensorType)
                        {
                            case SensorType.Data: //READ DATA RELATED TO RAM PRESENTED ON SYSTEM
                                {
                                    if (computer.Hardware[i].Sensors[j].Name == "Used Memory") //RAM OCCUPIED IN THE MOMENT
                                        miPC.ramused = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    if (computer.Hardware[i].Sensors[j].Name == "Available Memory") //FREE RAM ABLE TO USE
                                        miPC.ramavailable = miPC.floatqtointq(computer.Hardware[i].Sensors[j].Value);
                                    break;
                                }
                        }
                    }
                }
            }
            computer.Close();
            float? aux = miPC.ramavailable + miPC.ramused;
            return aux;
        }
        public my_pc get_()
        {
            return myPCmonitoring();
        }
    }
}
