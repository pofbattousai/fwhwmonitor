﻿namespace FWHWproject
{
    partial class fTemCirc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cpPlantilla = new CircularProgressBar.CircularProgressBar();
            this.SuspendLayout();
            // 
            // cpPlantilla
            // 
            this.cpPlantilla.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cpPlantilla.AnimationFunction = WinFormAnimation.KnownAnimationFunctions.CircularEaseOut;
            this.cpPlantilla.AnimationSpeed = 250;
            this.cpPlantilla.BackColor = System.Drawing.Color.Transparent;
            this.cpPlantilla.Font = new System.Drawing.Font("Razed", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpPlantilla.ForeColor = System.Drawing.Color.Lime;
            this.cpPlantilla.InnerColor = System.Drawing.Color.Black;
            this.cpPlantilla.InnerMargin = 2;
            this.cpPlantilla.InnerWidth = -1;
            this.cpPlantilla.Location = new System.Drawing.Point(12, 12);
            this.cpPlantilla.MarqueeAnimationSpeed = 250;
            this.cpPlantilla.MaximumSize = new System.Drawing.Size(320, 320);
            this.cpPlantilla.MinimumSize = new System.Drawing.Size(50, 50);
            this.cpPlantilla.Name = "cpPlantilla";
            this.cpPlantilla.OuterColor = System.Drawing.Color.Black;
            this.cpPlantilla.OuterMargin = -30;
            this.cpPlantilla.OuterWidth = 0;
            this.cpPlantilla.ProgressColor = System.Drawing.Color.Lime;
            this.cpPlantilla.ProgressWidth = 25;
            this.cpPlantilla.SecondaryFont = new System.Drawing.Font("Razed", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cpPlantilla.Size = new System.Drawing.Size(320, 320);
            this.cpPlantilla.StartAngle = 90;
            this.cpPlantilla.Step = 5;
            this.cpPlantilla.SubscriptColor = System.Drawing.Color.Lime;
            this.cpPlantilla.SubscriptMargin = new System.Windows.Forms.Padding(23, -15, 0, 0);
            this.cpPlantilla.SubscriptText = ".23";
            this.cpPlantilla.SuperscriptColor = System.Drawing.Color.Lime;
            this.cpPlantilla.SuperscriptMargin = new System.Windows.Forms.Padding(23, 25, 0, 0);
            this.cpPlantilla.SuperscriptText = "°C";
            this.cpPlantilla.TabIndex = 0;
            this.cpPlantilla.Text = "Nd";
            this.cpPlantilla.TextMargin = new System.Windows.Forms.Padding(-10, 8, 0, 0);
            this.cpPlantilla.Value = 100;
            // 
            // fTemCirc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(345, 343);
            this.ControlBox = false;
            this.Controls.Add(this.cpPlantilla);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(361, 382);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(180, 191);
            this.Name = "fTemCirc";
            this.Text = "mon_name";
            this.ResumeLayout(false);

        }

        #endregion

        public CircularProgressBar.CircularProgressBar cpPlantilla;
    }
}